<?
$items=array(
  "unixtime" => array(
         "desc" => "Дата публикации",
         "type" => "date",
         "fields" => array(
                      "d" => array("name" => "День","size" => "2", "after" => "."),
                      "m" => array("name" => "Месяц","size" => "2", "after" => "."),
                      "Y" => array("name" => "Год","size" => "4"),
                      "H" => array("name" => "Часы","size" => "2", "after" => ":"),
                      "i" => array("name" => "Минуты","size" => "2"),
                      ),
         "select_on_edit" => true,
         "select_on_edit_disabled" => true,
       ),

  "title" => array(
         "desc" => "Заголовок статьи",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
       ),


  "content" => array(
         "desc" => "Анонс статьи",
         "type" => "editor",
         "width" => "700",
         "height" => "500",
         "select_on_edit" => true,
       ),
  "more" => array(
         "desc" => "Содержание (подробнее)",
         "type" => "editor",
         "width" => "700",
         "height" => "500",
         "select_on_edit" => true,
       ),
  "name_title" => array(
         "desc" => "Название страницы в заголовке",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
//         "select_on_edit" => true,
       ),
  "classification" => array(
         "desc" => "Классификация страницы",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
       ),
  "descrip" => array(
         "desc" => "Описание страницы",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "textarea",
         "width" => "70",
         "height" => "5",
//         "select_on_edit" => true,
       ),
  "keywords" => array(
         "desc" => "Ключевые слова (через запятую)",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "textarea",
         "width" => "70",
         "height" => "5",
//         "select_on_edit" => true,
       ),
);
?>